﻿#pragma once

// Arkadia
#include "ark_core/ark_core.hpp"

namespace dfd {
//
//
//
class Demo
{
    //
    // Enums / Constants / Typedefs
    //
public:
    ARK_STRICT_CONSTEXPR static u32 DesiredWidth  = 320;
    ARK_STRICT_CONSTEXPR static u32 DesiredHeight = 200;
    ARK_STRICT_CONSTEXPR static u32 TargetFPS     = 60;

    //
    // Static Methods
    //
public:
    static ark::String GetWindowTitle();


    //
    // CTOR / DTOR
    //
public:
    ARK_STRICT_CONSTEXPR ARK_INLINE
    explicit Demo(f32 const target_update_time = 1 / f32(TargetFPS))
        : _target_update_time(target_update_time)
    {
        // Empty...
    }

public:

    ARK_INLINE bool IsRunning() const              { return _is_running;       }
    ARK_INLINE void SetIsRunning(bool is_running)  { _is_running = is_running; }

    ARK_INLINE void
    SetSize(u32 const width, u32 const height)
    {
        ARK_ASSERT_NOT_ZERO(width);
        ARK_ASSERT_NOT_ZERO(height);
        _fire_w = width;
        _fire_h = height;
        ark::FreeMemory(_fire_buffer);

        size_t const size = (width * height);
        _fire_buffer = ark::AllocMemory<u8>(size);
        ark::ResetMemory<u8>(_fire_buffer, size);
    }

    void Update(f32 const dt);
    void Render(ark::Gfx::OffscreenBuffer *offscreen_buffer);

private:
    bool  _is_running         = true;
    f32   _update_time        = 0;
    f32   _target_update_time = 0;

    u8   *_fire_buffer = nullptr;
    u32   _fire_w      = 0;
    u32   _fire_h      = 0;

    u32 _update_count = 0;
    u32 _render_count = 0;
    f32 _fps_time     = 1.0;
};  // class Demo

} // namespace dfd
